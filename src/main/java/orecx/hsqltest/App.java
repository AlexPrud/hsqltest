package orecx.hsqltest;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import orecx.hsqltest.database.search.UserSearchCriteria;
import orecx.hsqltest.entity.User;
import orecx.hsqltest.database.MemoryDatabase;

/**
 * Test app for hsqldb
 */
public class App {
    public static void main(String[] args) {
        MemoryDatabase database = null;

        try {
            database = new MemoryDatabase();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }

        insertTestUsers(database);
        ArrayList<User> users;
        users = database.readUsers(new UserSearchCriteria(null, null));
        showFilteredAndSortedUsers(users);
        users = database.readUsers(new UserSearchCriteria("ccc", 0));
        showFilteredAndSortedUsers(users);
    }

    private static void insertTestUsers(MemoryDatabase database) {
        User user;

        user = new User(0, "zero", "zerol", true);
        user.addLoginString("aaa");
        user.addLoginString("bbb");
        user.addLoginString("ccc");
        database.insertUser(user);

        user = new User(1, "one", "onel", false);
        user.addLoginString("ccc");
        user.addLoginString("ddd");
        database.insertUser(user);

        database.insertUser(new User(2, "two", "twol", null));
        database.insertUser(new User(3, "three", "threel", null));
        database.insertUser(new User(4, "four", "fourl", true));
    }

    private static void showFilteredAndSortedUsers(ArrayList<User> users) {
        users.stream().filter(user -> User.filterByNameContains(user, "one")).forEach((user) -> {
            System.out.println(user);
        });

        System.out.println("---");

        users.stream().sorted(User.getAscendingNameComparator()).forEach((user) -> {
            System.out.println(user);
        });

        System.out.println("---");

        users.stream().sorted(User.getRecordedComparator()).forEach((user) -> {
            System.out.println(user);
        });

        System.out.println("---");

        HashMap<String, ArrayList<User>> loginStrings = new HashMap<>();

        for (User user : users) {
            for (String loginString : user.getLoginStrings()) {
                loginStrings.putIfAbsent(loginString, new ArrayList<>());
                loginStrings.get(loginString).add(user);
            }
        }

        loginStrings.entrySet().stream().sorted(Comparator.comparing(Map.Entry::getKey)).forEach((entry) -> {
            for (User user : entry.getValue()) {
                System.out.println(entry.getKey() + " " + user);
            }
        });
    }
}
