package orecx.hsqltest.database;

import orecx.hsqltest.database.search.UserSearchCriteria;
import orecx.hsqltest.entity.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

public class MemoryDatabase extends Database {
    private static String USER_TABLE_NAME = "OrkUser";
    private static String LOGIN_STRING_TABLE_NAME = "OrkLoginString";

    public MemoryDatabase() throws ClassNotFoundException, SQLException {
        super();
    }

    @Override
    void initializeConnection() throws ClassNotFoundException, SQLException {
        Class.forName("org.hsqldb.jdbcDriver");
        connection = DriverManager.getConnection("jdbc:hsqldb:mem:orecx", "sa", "");
        createMemoryTables();
    }

    private void createMemoryTables() throws SQLException {
        createOrkUserTable();
        createOrkLoginStringTable();
    }

    private void createOrkUserTable() throws SQLException {
        Statement statement = connection.createStatement();
        statement.executeUpdate(String.format(
                "create table %s ("
                        + "id int primary key, "
                        + "firstName varchar(128), "
                        + "lastName varchar(128), "
                        + "recordable boolean"
                        + ")",
                USER_TABLE_NAME
        ));

        createIndex(USER_TABLE_NAME, "id");
        createIndex(USER_TABLE_NAME, "firstName");
        createIndex(USER_TABLE_NAME, "lastName");
        createIndex(USER_TABLE_NAME, "recordable");
    }

    private void createOrkLoginStringTable() throws SQLException {
        connection.createStatement().executeUpdate(String.format(
                "create table %s ("
                        + "loginString varchar(256), "
                        + "user_id int"
                        + ")",
                LOGIN_STRING_TABLE_NAME
        ));

        connection.createStatement().executeUpdate(String.format(
                "alter table %s add foreign key "
                        + "(user_id) references OrkUser (id) "
                        + "on delete cascade on update cascade",
                LOGIN_STRING_TABLE_NAME
        ));
    }

    private void createIndex(String tableName, String fieldName) throws SQLException {
        String query = String.format(
                "create index %s on %s (%s)",
                tableName + "_index_" + fieldName,
                tableName,
                fieldName
        );

        connection.createStatement().executeUpdate(query);
    }

    /**
     * Reads users matching search criteria.
     *
     * @param userSearchCriteria The search criteria for filtering.
     * @return The users in the database.
     */
    public ArrayList<User> readUsers(UserSearchCriteria userSearchCriteria) {
        ResultSet resultSet;
        ArrayList<User> users = new ArrayList<>();

        try {
            String query = String.format(
                    "select id, firstName, lastName, recordable, loginString from OrkUser "
                            + "full outer join %s on id=user_id where 1=1 ", LOGIN_STRING_TABLE_NAME
            );

            if (userSearchCriteria != null) {
                query += userSearchCriteria.generatedPreparedWhere();
            }

            PreparedStatement preparedStatement = connection.prepareStatement(query);

            if (userSearchCriteria != null) {
                userSearchCriteria.assignPreparedStatementValues(1, preparedStatement);
            }

            resultSet = preparedStatement.executeQuery();
            populateUsersList(users, resultSet);
        } catch (SQLException e) {
            // Log error
            e.printStackTrace();
        }

        return users;
    }

    private void populateUsersList(ArrayList<User> users, ResultSet resultSet) throws SQLException {
        HashMap<Integer, User> mappedUsers = new HashMap<>();
        User user;

        while (resultSet.next()) {
            int id = resultSet.getInt("id");

            if (!mappedUsers.containsKey(id)) {
                // getObject (instead of getBoolean) allows for null values
                user = new User(id,
                        resultSet.getString("firstName"),
                        resultSet.getString("lastName"),
                        (Boolean) resultSet.getObject("recordable"));
                mappedUsers.put(id, user);
                users.add(user);
            } else {
                user = mappedUsers.get(id);
            }

            String loginString = resultSet.getString("loginString");
            user.addLoginString(loginString);
        }
    }

    /**
     * Inserts a single user.
     *
     * @param user The user to insert.
     * @return true if user has successfully been inserted, false otherwise.
     */
    public Boolean insertUser(User user) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(String.format(
                    "insert into %s (id, firstName, lastName, recordable) values (?, ?, ?, ?);",
                    USER_TABLE_NAME
            ));
            preparedStatement.setInt(1, user.getId());
            preparedStatement.setString(2, user.getFirstName());
            preparedStatement.setString(3, user.getLastName());
            // setObject allows for null values
            preparedStatement.setObject(4, user.getRecordable());

            int count = preparedStatement.executeUpdate();

            insertLoginString(user);

            return count == 1;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    private void insertLoginString(User user) throws SQLException {
        for (String loginString : user.getLoginStrings()) {
            PreparedStatement preparedStatement = connection.prepareStatement(String.format(
                    "insert into %s (loginString, user_id) values (?, ?);",
                    LOGIN_STRING_TABLE_NAME
            ));

            preparedStatement.setString(1, loginString);
            preparedStatement.setInt(2, user.getId());

            preparedStatement.executeUpdate();
        }
    }
}
