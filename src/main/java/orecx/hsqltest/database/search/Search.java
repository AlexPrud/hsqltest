package orecx.hsqltest.database.search;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface Search {
    /**
     * Generates the where part of a query.
     * @return The where part of the query.
     */
    String generatedPreparedWhere();

    /**
     * Assigns prepared statement values based on search.
     * @param starting The starting index of the prepared statement value to assign.
     * @param preparedStatement The prepared statement to assign the values to.
     */
    void assignPreparedStatementValues(int starting, PreparedStatement preparedStatement) throws SQLException;
}
