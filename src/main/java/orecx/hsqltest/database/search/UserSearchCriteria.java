package orecx.hsqltest.database.search;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;

public class UserSearchCriteria implements Search {

    private final Optional<String> loginString;
    private final Optional<Integer> userId;

    public UserSearchCriteria(String loginString, Integer userId) {
        this.loginString = loginString == null ? Optional.empty() : Optional.of(loginString);
        this.userId = userId == null ? Optional.empty() : Optional.of(userId);
    }

    @Override
    public String generatedPreparedWhere() {
        String where = "";

        if (userId.isPresent()) {
            where += " and id=? ";
        }

        if (loginString.isPresent()) {
            where += " and loginString=? ";
        }

        return where;
    }

    @Override
    public void assignPreparedStatementValues(int starting, PreparedStatement preparedStatement) throws SQLException {
        if (userId.isPresent()) {
            preparedStatement.setInt(starting++, userId.get());
        }

        if (loginString.isPresent()) {
            preparedStatement.setString(starting++, loginString.get());
        }
    }
}
