package orecx.hsqltest.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import orecx.hsqltest.entity.User;

public abstract class Database {

	protected Connection connection;

	public Database() throws ClassNotFoundException, SQLException {
		initializeConnection();
	}

	/**
	 * Initializes the database connection.
	 * @throws ClassNotFoundException if there's a classpath issue.
	 * @throws SQLException if there's a generic SQL issue.
	 */
	abstract void initializeConnection() throws ClassNotFoundException, SQLException;
}
