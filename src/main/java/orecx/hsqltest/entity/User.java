package orecx.hsqltest.entity;

import java.util.*;

public class User {
    private Integer id;
    private String firstName;
    private String lastName;
    private Boolean recordable;
    private Set<String> loginStrings;

    private static Comparator<User> ascendingNameComparator;
    private static Comparator<User> recordedComparator;

    public User(int id, String firstName, String lastName, Boolean recordable) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.recordable = recordable;
        this.loginStrings = new HashSet<>();
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Boolean getRecordable() {
        return recordable;
    }

    public HashSet<String> getLoginStrings() {
        return new HashSet<>(loginStrings);
    }

    public void addLoginString(String loginString) {
        if (loginString != null) {
            this.loginStrings.add(loginString);
        }
    }

    @Override
    public String toString() {
        String toString = String.join(" ",
                id.toString(),
                firstName,
                lastName,
                recordable != null ? recordable.toString() : null
        );

        if (loginStrings.size() > 0) {
            toString += " " + loginStrings.toString();
        }

        return toString;
    }

    public static Boolean filterByNameContains(User user, String nameContains) {
        return user.firstName.contains(nameContains);
    }

    public static Comparator<User> getAscendingNameComparator() {
        if (ascendingNameComparator == null) {
            ascendingNameComparator = Comparator.comparing(user -> user.firstName);
        }

        return ascendingNameComparator;
    }

    public static Comparator<User> getRecordedComparator() {
        if (recordedComparator == null) {
            recordedComparator = (first, second) -> {
                if (first.recordable == null) {
                    return 1;
                } else if (second.recordable == null) {
                    return -1;
                } else {
                    return first.recordable.compareTo(second.recordable);
                }
            };
        }

        return recordedComparator;
    }
}
